import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './authentication/login.component';
import { TodoListComponent } from './todoList/todolist.component';
import { AuthGuards } from './services/auth.guards';

const routes: Routes = [
  { path: '', component: TodoListComponent, canActivate: [AuthGuards] },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }