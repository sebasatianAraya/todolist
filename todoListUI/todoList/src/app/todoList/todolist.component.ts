import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { TodoListService } from '../services/todoList.component';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Component({
    selector: 'todolist',
    templateUrl: './todolist.component.html'
  })
  export class TodoListComponent implements OnInit {
    private addTaskForm: FormGroup;
    errorMessage = "";
    listTasks: any;

    constructor(
        private formBuilder: FormBuilder,
        private todoListService: TodoListService,
        private authenticationService: AuthenticationService) {}
        
    ngOnInit() {
      this.addTaskForm = this.formBuilder.group({
        'text': new FormControl('', Validators.required)
      });
      this.getAllTasks();
    }
    onSubmitNewTask(data){
       this.todoListService.createTask(data).pipe(first()).subscribe(
        response => {
          this.listTasks = response;
          this.addTaskForm.reset();
        },
        ex => {
          console.log("ERROR")
          console.log(ex)
        });
    }
    getAllTasks(){
      this.todoListService.getAll().pipe(first()).subscribe(
        data => {
          this.listTasks = data;
        },
        ex => {
          console.log("ERROR")
          console.log(ex)
        });
    }
    remove(task){
      this.todoListService.delete(task._id).pipe(first()).subscribe(
        data => {
          console.log(data)
          this.listTasks = this.listTasks.filter( n => n._id!==task._id);
          return 
        },
        ex => {
          console.log("ERROR");
          console.log(ex);
        });
    }
}