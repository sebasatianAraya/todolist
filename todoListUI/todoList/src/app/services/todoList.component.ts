import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

//import { User } from '../_models';

@Injectable()
export class TodoListService {
    constructor(private http: HttpClient) { }
    //TODO: create parameter for url config.
    private configUrl = "http://localhost:3000/api/tasks"
    
    getAll() {
        return this.http.get<any>(`${this.configUrl}`);
    }
    createTask(data: any) {
        return this.http.post(`${this.configUrl}`, data);
    }

    delete(id: number) {
        return this.http.delete(`${this.configUrl}/` + id);
    }
}