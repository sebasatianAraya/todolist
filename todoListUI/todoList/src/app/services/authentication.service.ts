import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { headersToString } from 'selenium-webdriver/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient, private router:Router ) { }

    login(username: string, password: string) {
        //TODO: create parameter for url config.
        return this.http.post<any>(` http://localhost:3000/api/login`, 
            { username: username, password: password })
            .pipe(map(user => {
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }
}