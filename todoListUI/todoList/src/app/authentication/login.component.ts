import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { first } from 'rxjs/operators';
import { resetCompiledComponents } from '@angular/core/src/render3/jit/module';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  errorMessage = "";
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService) {}

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
        'username': new FormControl('test', Validators.required),
        'password': new FormControl('asdf123', Validators.required)
      });
      this.authenticationService.logout();
  }

  onSubmit(data:any) {
      this.submitted = true;
      this.authenticationService
        .login(data.username, data.password)
        .pipe(first())
        .subscribe(
          data => {
            this.reset();
            this.router.navigate(['/']);
          },
          ex => {
            this.errorMessage = ex.error.err
            console.log("ERROR Login")
            console.log(ex)
          });
  }
  
  reset(){
    this.loginForm.reset();
    this.errorMessage = "";
  }
}