var express = require('express');			
var mongoose = require('mongoose'); 					
var config = require('./config'); 			
var bodyParser = require('body-parser');
var task = require('./app/task/task');
var login = require('./app/authentication/login')

var app = express(); 	

mongoose.connect(config.database.mongoConnection, {useNewUrlParser: true});

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));
/*
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
    next();
});
*/

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Headers', 'Content-type,Authorization');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use("/api/login", login);
app.use("/api/tasks", task);

app.listen(config.server.port);
console.log("App listening on port " + config.server.port);

