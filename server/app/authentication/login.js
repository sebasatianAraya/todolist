var express = require("express")
var jwtMW = require('./../../jwtMW');
var jwt = require('jsonwebtoken');
var config = require('./../../config');

var login = express.Router();

//TODO: agregar find by user para log

let users = [
    {
        id: 1,
        username: 'test',
        password: 'asdf123'
    },
    {
        id: 2,
        username: 'test2',
        password: 'asdf12345'
    }
];

login.post('/', (req, res) => {
    const { username, password } = req.body;
    for (let user of users) { 
        if (username == user.username && password == user.password ) {
            let token = jwt.sign({ id: user.id, username: user.username }, config.server.key, { expiresIn: 129600 }); // Sigining the token
            res.json({
                sucess: true,
                err: null,
                token
            });
            break;
        }
        else {
            res.status(401).json({
                sucess: false,
                token: null,
                err: 'Username or password is incorrect'
            });
        }
    }
});

module.exports = login;