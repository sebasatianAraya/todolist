var express = require("express")
var Task = require('./../model/Task');
var jwtMW = require('./../../jwtMW');
var jwt = require('jsonwebtoken');
var taskApi = express.Router();

const PENDING= "pending";
const READY= "ready";

taskApi.get('/', jwtMW, (req, res) => getTasks(req, res));

taskApi.post('/', jwtMW, (req, res) => {
    Task.create({
        text: req.body.text,
        username: req.user.username,
        status: PENDING
    },(err) =>{
        if (err){
            res.send(err);
        }
        getTasks(req, res);
    });
});

taskApi.delete('/:task_id', jwtMW, function (req, res) {
    Task.deleteOne({_id: req.params.task_id})
        .then((err) => getTasks(res))
        .catch((err) => res.send(err));
});

function getTasks(req, res) {
    Task.find({username: req.user.username},
        (err, Tasks)=>{
        if (err) {
            res.send(err);
        }
        else
            res.json(Tasks); 
    });
};

taskApi.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send(err);
    }
    else {
        next(err);
    }
});

module.exports = taskApi;