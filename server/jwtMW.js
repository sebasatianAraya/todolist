var exjwt = require('express-jwt');
var config = require('./config');

var jwtMW = exjwt({
    secret: config.server.key
});

module.exports = jwtMW